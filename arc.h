//
// Created by Irina Vergunova on 2019-11-25.
//

#ifndef LABA3SFML_ARC_H
#define LABA3SFML_ARC_H
#include "voronoi.h"

class Event;

struct Arc
{
    enum class Color{RED, BLACK};

    // Hierarchy
    Arc* parent;
    Arc* left;
    Arc* right;
    // Diagram
    VoronoiDiagram::Site* site;
    VoronoiDiagram::HalfEdge* leftHalfEdge;
    VoronoiDiagram::HalfEdge* rightHalfEdge;
    Event* event;
    // Optimizations
    Arc* prev;
    Arc* next;
    // Only for balancing
    Color color;
};
#endif //LABA3SFML_ARC_H
